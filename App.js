/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View,
  Image,
  Button,
  Alert,
} from 'react-native';
const App = () => {
  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView>
        <View style={styles.box}>
          <Image style={styles.circle} source={require('./image/logo.png')} />
        </View>

        <View>
          <Button
            title="Kişi Ekle"
            onPress={() => Alert.alert('...' + ' kişisi eklendi')}
          />
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: '#eaeaea',
  },
  box: {
    ...StyleSheet.absoluteFill,
    width: 500,
    height: 200,
    backgroundColor: '#EE161F',
  },
  circle: {
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    backgroundColor: 'red',
    position: 'absolute',
    top: 50,
    left: 130,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textinput: {
    width: 50,
    height: 50,
    top: 200,
    borderColor: 'gray',
    borderWidth: 1,
  },

  title: {
    marginTop: 200,
    color: '#20232a',
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
});

export default App;
